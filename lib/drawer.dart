import 'package:flutter/material.dart';

class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text(
              '菜单',
              style: TextStyle(color: Colors.white, fontSize: 25),
            ),
            decoration: BoxDecoration(
                color: Colors.green,
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/cover.jpg'))),
          ),
          ListTile(
            leading: Icon(Icons.home_rounded),
            title: Text('主页'),
            onTap: () => {},
          ),
          ExpansionTile(
            leading: Icon(Icons.apps_rounded),
            title: Text('模式案例'),
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.menu_rounded),
                title: Text('子菜单'),
                onTap: () => {Navigator.of(context).pop()},
              ),
              ListTile(
                leading: Icon(Icons.menu_rounded),
                title: Text('子菜单'),
                onTap: () => {Navigator.of(context).pop()},
              ),
              ListTile(
                leading: Icon(Icons.menu_rounded),
                title: Text('子菜单'),
                onTap: () => {Navigator.of(context).pop()},
              ),
            ],
          ),
          ListTile(
            leading: Icon(Icons.architecture),
            title: Text('在线定制'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ExpansionTile(
            leading: Icon(Icons.admin_panel_settings),
            title: Text('资源库'),
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.menu_rounded),
                title: Text('子菜单'),
                onTap: () => {Navigator.of(context).pop()},
              ),
              ListTile(
                leading: Icon(Icons.menu_rounded),
                title: Text('子菜单'),
                onTap: () => {Navigator.of(context).pop()},
              ),
              ListTile(
                leading: Icon(Icons.menu_rounded),
                title: Text('子菜单'),
                onTap: () => {Navigator.of(context).pop()},
              ),
            ],
          ),
          ListTile(
            leading: Icon(Icons.library_books),
            title: Text('知识库'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.question_answer_outlined),
            title: Text('问答'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ExpansionTile(
            leading: Icon(Icons.admin_panel_settings),
            title: Text('管理'),
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.supervisor_account),
                title: Text('用户管理'),
                onTap: () => {Navigator.of(context).pop()},
              ),
              ListTile(
                leading: Icon(Icons.book_online_outlined),
                title: Text('订单管理'),
                onTap: () => {Navigator.of(context).pop()},
              ),
            ],
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('登出'),
            onTap: () => {Navigator.of(context).pop()},
          ),
        ],
      ),
    );
  }
}