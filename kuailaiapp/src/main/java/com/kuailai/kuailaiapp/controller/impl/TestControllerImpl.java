package com.kuailai.kuailaiapp.controller.impl;import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.kuailai.kuailaiapp.bean.response.login.MessageResponse;
import com.kuailai.kuailaiapp.controller.TestController;

@Component
public class TestControllerImpl implements TestController {

	@Override
	public String allAccess() {
		// TODO Auto-generated method stub
		return "Public Content.";
	}

	@Override
	public String userAccess() {
		// TODO Auto-generated method stub
		return "User Content.";
	}

	@Override
	public ResponseEntity<?> moderatorAccess() {
		// TODO Auto-generated method stub
		return ResponseEntity.ok(new MessageResponse("Mod content"));
	}

	@Override
	public String adminAccess() {
		// TODO Auto-generated method stub
		return "Admin Board.";
	}

}
