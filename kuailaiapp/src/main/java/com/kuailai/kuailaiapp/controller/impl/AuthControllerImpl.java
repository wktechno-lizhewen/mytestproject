package com.kuailai.kuailaiapp.controller.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.kuailai.kuailaiapp.bean.request.login.LoginRequest;
import com.kuailai.kuailaiapp.bean.request.login.SignupRequest;
import com.kuailai.kuailaiapp.bean.request.login.TokenRefreshRequest;
import com.kuailai.kuailaiapp.bean.response.login.JwtResponse;
import com.kuailai.kuailaiapp.bean.response.login.MessageResponse;
import com.kuailai.kuailaiapp.bean.response.login.TokenRefreshResponse;
import com.kuailai.kuailaiapp.controller.AuthController;
import com.kuailai.kuailaiapp.exception.TokenRefreshException;
import com.kuailai.kuailaiapp.model.user.ERole;
import com.kuailai.kuailaiapp.model.user.RefreshToken;
import com.kuailai.kuailaiapp.model.user.Role;
import com.kuailai.kuailaiapp.model.user.User;
import com.kuailai.kuailaiapp.repository.user.RoleRepository;
import com.kuailai.kuailaiapp.repository.user.UserRepository;
import com.kuailai.kuailaiapp.security.jwt.JwtUtils;
import com.kuailai.kuailaiapp.security.service.RefreshTokenService;
import com.kuailai.kuailaiapp.security.service.UserDetailsImpl;

@Component
public class AuthControllerImpl implements AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;
	
	@Autowired
	RefreshTokenService refreshTokenService;
	
	@Override
	public ResponseEntity<?> authenticateUser(@Valid LoginRequest loginRequest) {
		// TODO Auto-generated method stub
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());

		RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getId());
		
		return ResponseEntity.ok(new JwtResponse(jwt,
												 refreshToken.getToken(),
												 userDetails.getId(), 
												 userDetails.getUsername(), 
												 userDetails.getEmail(), 
												 roles));
		
	}

	@Override
	public ResponseEntity<?> registerUser(@Valid SignupRequest signUpRequest) {
		// TODO Auto-generated method stub
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		User user = new User(signUpRequest.getUsername(), 
							 signUpRequest.getEmail(),
							 encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
				case "mod":
					Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modRole);

					break;
				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}

		user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}

	@Override
	public ResponseEntity<?> refreshToken(@Valid TokenRefreshRequest request) {
		// TODO Auto-generated method stub
		String requestRefreshToken = request.getRefreshToken();
		
		return refreshTokenService.findByToken( requestRefreshToken )
						   		  .map( refreshTokenService::verifyExpiration )
						   		  .map(RefreshToken::getUser)
						   		  .map( user ->
						   		  			{ 
						   			  			String token = jwtUtils.generateTokenFromUsername(user.getUsername());
						   			  			return ResponseEntity.ok(new TokenRefreshResponse(token, requestRefreshToken)); 
						   		  			}
						   		  )
						   		  .orElseThrow(() -> new TokenRefreshException(requestRefreshToken,"Refresh token is not in database!"));

	}

}
