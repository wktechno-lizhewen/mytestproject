package com.kuailai.kuailaiapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KuailaiappApplication {

	public static void main(String[] args) {
		SpringApplication.run(KuailaiappApplication.class, args);
	}

}
