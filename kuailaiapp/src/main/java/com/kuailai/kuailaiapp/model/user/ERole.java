package com.kuailai.kuailaiapp.model.user;

public enum ERole {

	ROLE_USER,
	ROLE_MODERATOR,
	ROLE_ADMIN
}
